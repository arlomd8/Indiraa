﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class destroy : MonoBehaviour
{
    private Button touch;
    public GameObject sound;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(Detect);
    }

    void Detect()
    {
        sound.SetActive(false);
        Destroy(sound);
    }
}
