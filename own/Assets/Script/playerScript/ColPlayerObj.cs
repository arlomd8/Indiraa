﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColPlayerObj : MonoBehaviour
{
    public Text photo;
    public Text nyawaText;
    public int nilai = -1;
    public int nyawa = 3;

    void Update()
    {
        if (nyawa == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("photo"))
        {
            Destroy(col.gameObject);
            nilai++;
            photo.text = ("PHOTOS : " + nilai.ToString());
        }
        if (col.gameObject.tag.Equals("ghost"))
        {
            nyawa--;
            nyawaText.text = ("NYAWA : " + nyawa.ToString());
        }
        if (col.gameObject.tag.Equals("bed") && nilai >= 19)
        {
            SceneManager.LoadScene("Finish");
        }
    }
}
