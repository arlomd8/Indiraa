﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class hideUnhide : MonoBehaviour
{
    public GameObject artGrey;
    public GameObject artColour;
    public GameObject ghost;
    public Text skillText;
    int skill = 10;
    
    // Update is called once per frame
    void Update()
    {
        StartCoroutine(SkillFunc());
    }

    IEnumerator SkillFunc()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (skill == 10)
            {
                ghost.SetActive(false);
                artGrey.SetActive(false);
                artColour.SetActive(true);
                for (int i = 10; i > 0; i--)
                {
                    skill--;
                    skillText.text = ("COOLDOWN : " + skill.ToString());
                    yield return new WaitForSeconds(1.5f);
                }
            }
        }

        if (skill == 0)
        {
            ghost.SetActive(true);
            artGrey.SetActive(true);
            artColour.SetActive(false);
            for (int i = 0; i < 10; i++)
            {
                skill++;
                skillText.text = ("COOLDOWN : " + skill.ToString());
                yield return new WaitForSeconds(1.5f);
            }
        }
    }
}
