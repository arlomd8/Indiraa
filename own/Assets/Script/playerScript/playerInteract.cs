﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerInteract : MonoBehaviour
{
    public GameObject currentIntObject = null;

    void Update()
    {
            if(Input.GetKey(KeyCode.X) && currentIntObject)
        {
            //Do something with the object
            currentIntObject.SendMessage("DoInteraction");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag ("item"))
        { 
            Debug.Log(col.name);
            currentIntObject = col.gameObject;
            
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("item"))
        {
            if(col.gameObject == currentIntObject)
            {
                currentIntObject = null;
            }
           
        } 
    }
}
