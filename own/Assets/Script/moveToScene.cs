﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class moveToScene : MonoBehaviour
{
    private Button touch;
    public string sceneName = string.Empty;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(Detect);
    }

    void Detect()
    {
        SceneManager.LoadScene(sceneName);
    }
}
