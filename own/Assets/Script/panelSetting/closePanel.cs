﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class closePanel : MonoBehaviour
{
    private Button close;
    public GameObject panelSetting;

    // Start is called before the first frame update
    void Start()
    {
        close = GetComponent<Button>();
        close.onClick.AddListener(Detect);
    }

    private void Detect()
    {
        panelSetting.SetActive(false);
    }
}
